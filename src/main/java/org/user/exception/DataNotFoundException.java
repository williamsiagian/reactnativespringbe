package org.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Invalid email/password")
public class DataNotFoundException extends RuntimeException{

	public DataNotFoundException(String message) {

        super(message);
    }
}
