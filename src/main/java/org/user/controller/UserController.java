package org.user.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.user.exception.DataNotFoundException;
import org.user.model.User;
import org.user.repository.UserRepository;

@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserRepository repository;
	
	@PostMapping("/login")
	public ResponseEntity<User> loginUser(@RequestBody User user) {
		String email = user.getEmail();
		String password = user.getPassword();
		
		Optional<User> userData = repository.findByEmailAndPassword(email, password);

		if (userData.isPresent()) {
			return new ResponseEntity<>(userData.get(), HttpStatus.OK);
		} else {
            throw new DataNotFoundException("Data Not Found");
		}
	}
	
//	@PostMapping("/login")
//    public User loginUser(@RequestBody User user) {
//        String email = user.getEmail();
//        String password = user.getPassword();
//        
//        if(!email.equals(user.getEmail()) && !password.equals(user.getPassword()))
//        	System.out.println("Invalid email/password");
//        
//        return repository.findByEmailAndPassword(email, password);
//    }
	
	@PostMapping("/register")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		try {
			User _user = repository.save(new User(user.getEmail(), user.getUsername(), user.getPassword()));
			return new ResponseEntity<>(_user, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
